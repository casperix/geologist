package ru.casperix

import io.github.xn32.json5k.SerialComment
import kotlinx.serialization.Serializable

@Serializable
class GeologistConfig(
	val analyze: GeologistAnalyzeConfig = GeologistAnalyzeConfig(),
	val lowKit: GeologistKitConfig = GeologistKitConfig(OreConfigList.LOW),
	val mediumKit: GeologistKitConfig = GeologistKitConfig(OreConfigList.LOW + OreConfigList.MEDIUM),
	val highKit: GeologistKitConfig = GeologistKitConfig(OreConfigList.LOW + OreConfigList.MEDIUM + OreConfigList.HIGH),
)

@Serializable
class GeologistAnalyzeConfig(

	@SerialComment("first step")
	val stepMin: Int = 0,

	@SerialComment("last step")
	val stepMax: Int = 31,

	@SerialComment("calculate next range:\ncurrentRange += 1 + currentStep / stepDivider")
	val stepDivider: Int = 6,

	val normalizer: Float = 1f,
)

@Serializable
class GeologistKitConfig(val allowedBlockNames: List<String>)