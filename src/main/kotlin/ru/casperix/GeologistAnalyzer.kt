package ru.casperix

import net.minecraft.block.Block
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import ru.casperix.math.Box3FaceIterator
import ru.casperix.math.Box3i
import ru.casperix.math.Vector3i
import ru.casperix.util.toBlockPos
import ru.casperix.util.toVector3i
import kotlin.math.max
import kotlin.math.min

object GeologistAnalyzer {
	fun analyze(world: World, startPos: BlockPos, targetBlock: Block): Float {
		val analyze = GeologistConfigProvider.get().analyze
		var rawSignal = 0f

		val stepIndices = analyze.stepMin..analyze.stepMax
		val stepDivider = analyze.stepDivider
		val normalizer = analyze.normalizer

		var range = 0
		stepIndices.forEach { stepIndex ->
			val stepSize = 1 + stepIndex / stepDivider
			range += stepSize

			val center = startPos.toVector3i()
			val oreWeight = 1f / range.toFloat()
			Box3FaceIterator(Box3i.byRadius(center, Vector3i(range)), stepSize).forEach { position ->
				val blockState = world.getBlockState(position.toBlockPos())
				if (blockState.block == targetBlock) {
					rawSignal += oreWeight
				}
			}
		}

		return max(0f, min(100f, (rawSignal * normalizer)))
	}
}