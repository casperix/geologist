package ru.casperix.math

class Circular3Iterator(val center: Vector3i, val minRange: Int, val maxRange: Int, val step: Int = 1) : Iterator<Vector3i> {
	var position: Vector3i? = null
	var lastRadius = minRange - 1
	var iterator: Box3FaceIterator? = null

	init {
		if (step < 1) throw Error("Step must be greater or equal than one (now: $step)")
		if (minRange < 0) throw Error("Min radius must be non negative (now: $minRange)")
		if (maxRange < 0) throw Error("Max radius must be non negative (now: $maxRange)")
		if (minRange > maxRange) throw Error("Min radius ($minRange) must be non greater than max radius ($maxRange)")

		nextIterator()
		position = getNext()
	}

	private fun nextIterator() {
		lastRadius += step
		if (lastRadius <= maxRange) {
			iterator = Box3FaceIterator(Box3i.byRadius(center, Vector3i(lastRadius)), step)
		} else {
			iterator = null
		}
	}

	private fun getNext(): Vector3i? {
		while (true) {
			val iterator = iterator ?: return null
			if (iterator.hasNext()) {
				return iterator.next()
			} else {
				nextIterator()
			}
		}
	}

	override fun next(): Vector3i {
		val last = position ?: throw Error("Invalid next. You must check hasNext first")
		position = getNext()
		return last
	}

	override fun hasNext(): Boolean {
		return position != null
	}

}