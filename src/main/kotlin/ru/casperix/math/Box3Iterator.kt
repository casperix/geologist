package ru.casperix.math


class Box3Iterator(val source: Box3i, val step: Int = 1) : Iterator<Vector3i> {
	private var x: Int
	private var y: Int
	private var z: Int
	private var next: Vector3i?

	init {
		x = source.min.x
		y = source.min.y
		z = source.min.z
		next = Vector3i(x, y, z)
	}

	/**
	 * Returns the next element in the iteration.
	 */
	override operator fun next(): Vector3i {
		val result = next!!

		x += step
		if (x > source.max.x) {
			x = source.min.x
			y += step
			if (y > source.max.y) {
				y = source.min.y
				z += step
				if (z > source.max.z) {
					next = null
					return result
				}
			}
		}
		next = Vector3i(x, y, z)
		return result
	}


	/**
	 * Returns `true` if the iteration has more elements.
	 */
	override operator fun hasNext(): Boolean {
		return next != null
	}
}