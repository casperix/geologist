package ru.casperix.math


/**
 * 	Итератор по поверхности бокса
 */
class Box3FaceIterator(val source: Box3i, val step: Int = 1) : Iterator<Vector3i> {
	private var x: Int
	private var y: Int
	private var z: Int
	private var next: Vector3i?
	private var xStep = step
	private var bigXStep = source.max.x - source.min.x

	init {
		x = source.min.x
		y = source.min.y
		z = source.min.z
		next = Vector3i(x, y, z)
	}

	/**
	 * Returns the next element in the iteration.
	 */
	override operator fun next(): Vector3i {
		val result = next!!

		while (true) {
			x += xStep
			if (x > source.max.x) {
				x = source.min.x
				y += step
				updateXStep()
				if (y > source.max.y) {
					y = source.min.y
					z += step
					updateXStep()
					if (z > source.max.z) {
						next = null
						return result
					}
				}
			}
			val pos = Vector3i(x, y, z)
			if (source.isSide(pos)) {
				next = pos
				return result
			}
		}
	}

	private fun updateXStep() {
		if ((z == source.min.z || z == source.max.z) || (y == source.min.y || y == source.max.y)) {
			xStep = step
		} else {
			xStep = bigXStep
		}
	}


	/**
	 * Returns `true` if the iteration has more elements.
	 */
	override operator fun hasNext(): Boolean {
		return next != null
	}
}