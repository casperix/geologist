package ru.casperix.math

/*
 * 	Axis aligned box
 */
interface Box<Vertex, Type> {
	val min: Vertex
	val max: Vertex
	val center: Vertex
	val dimension: Vertex
	val volume: Type

	fun isInside(point: Vertex): Boolean

	fun isOutside(point: Vertex): Boolean {
		return !isInside(point)
	}
}