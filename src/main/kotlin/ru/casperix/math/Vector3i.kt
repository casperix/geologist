package ru.casperix.math

import kotlinx.serialization.Serializable
import kotlin.math.*

data class Vector3i(val x: Int, val y: Int, val z: Int) {
	constructor() : this(0)

	constructor(i: Int) : this(i, i, i)

	companion object {
		val ZERO = Vector3i(0)
		val ONE = Vector3i(1)
		val XYZ = ONE

		val X = Vector3i(1, 0, 0)
		val Y = Vector3i(0, 1, 0)
		val Z = Vector3i(0, 0, 1)
		val YZ = Vector3i(0, 1, 1)
		val XZ = Vector3i(1, 0, 1)
		val XY = Vector3i(1, 1, 0)
	}

	val xAxis: Vector3i get() = Vector3i(x, 0, 0)
	val yAxis: Vector3i get() = Vector3i(0, y, 0)
	val zAxis: Vector3i get() = Vector3i(0, 0, z)

	fun volume(): Int {
		return x * y * z
	}

	fun destTo(other: Vector3i):Double {
		return (this - other).length()
	}

	fun lengthOne(): Int {
		return abs(x) + abs(y) + abs(z)
	}

	fun length(): Double {
		return sqrt((x * x + y * y + z * z).toDouble())
	}

	fun lengthInf(): Int {
		return maxOf(abs(x), abs(y), abs(z))
	}

	fun lengthSquared(): Int {
		return x * x + y * y + z * z
	}

	fun absoluteMinimum(): Int {
		return minOf(abs(x), abs(y), abs(z))
	}

	fun absoluteMaximum(): Int {
		return maxOf(abs(x), abs(y), abs(z))
	}

	val sign: Vector3i get() = Vector3i(x.sign, y.sign, z.sign)

	val absoluteValue: Vector3i get() = Vector3i(x.absoluteValue, y.absoluteValue, z.absoluteValue)

	fun dot(value: Vector3i): Int {
		return (this.x * value.x + this.y * value.y + this.z * value.z)
	}

	fun upper(other: Vector3i): Vector3i {
		return Vector3i(max(x, other.x), max(y, other.y), max(z, other.z))
	}

	fun lower(other: Vector3i): Vector3i {
		return Vector3i(min(x, other.x), min(y, other.y), min(z, other.z))
	}

	fun clamp(min: Vector3i, max: Vector3i): Vector3i {
		return upper(min).lower(max)
	}


	fun clamp(area: Box3i): Vector3i {
		return clamp(area.min, area.max)
	}

	operator fun plus(position: Vector3i): Vector3i {
		return Vector3i(x + position.x, y + position.y, z + position.z)
	}

	operator fun minus(position: Vector3i): Vector3i {
		return Vector3i(x - position.x, y - position.y, z - position.z)
	}

	operator fun div(value: Int): Vector3i {
		return Vector3i(x / value, y / value, z / value)
	}

	operator fun div(value: Vector3i): Vector3i {
		return Vector3i(x / value.x, y / value.y, z / value.z)
	}

	operator fun times(value: Int): Vector3i {
		return Vector3i(x * value, y * value, z * value)
	}

	operator fun times(value: Vector3i): Vector3i {
		return Vector3i(x * value.x, y * value.y, z * value.z)
	}

	operator fun unaryMinus(): Vector3i {
		return Vector3i(-x, -y, -z)
	}

	operator fun rem(value: Vector3i): Vector3i {
		return Vector3i(x % value.x, y % value.y, z % value.z)
	}

	operator fun rem(value: Int): Vector3i {
		return Vector3i(x % value, y % value, z % value)
	}

	fun greater(other: Vector3i): Boolean {
		return x > other.x && y > other.y && z > other.z
	}

	fun greaterOrEq(other: Vector3i): Boolean {
		return x >= other.x && y >= other.y && z >= other.z
	}

	fun less(other: Vector3i): Boolean {
		return x < other.x && y < other.y && z < other.z
	}

	fun lessOrEq(other: Vector3i): Boolean {
		return x <= other.x && y <= other.y && z <= other.z
	}

	override fun toString(): String {
		return "V3i($x,$y,$z)"
	}


	fun component(index:Int):Int {
		return when(index) {
			0->x
			1->y
			2->z
			else->throw Error("Only 3 components enabled")
		}
	}
}


