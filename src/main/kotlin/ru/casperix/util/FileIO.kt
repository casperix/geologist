package ru.casperix.util

import java.io.File
import java.nio.file.Paths


fun saveText(fileName: String, data: String) {

	val path = Paths.get(fileName)
	val file = path.toFile()

	if (!file.exists()) {
		createDirectoryForFile(file)
	}
	file.writeText(data)
}

fun createDirectoryForFile(file: File) {
	if (!file.parentFile.exists()) {
		file.parentFile.mkdirs()
	}
}