package ru.casperix.util

import net.minecraft.util.math.BlockPos
import ru.casperix.math.Vector3i

fun BlockPos.toVector3i(): Vector3i {
	return Vector3i(x, y, z)
}

fun Vector3i.toBlockPos(): BlockPos {
	return BlockPos(x, y, z)
}