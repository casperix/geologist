package ru.casperix

import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.fabricmc.loader.api.FabricLoader
import net.fabricmc.loader.api.MappingResolver
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import org.slf4j.LoggerFactory

object GeologistMod : ModInitializer {
	val logger = LoggerFactory.getLogger("geologist")
	var config = GeologistConfig()


	init {
		Registry.register(Registry.ITEM, Identifier("geologist", "geologist_kit1"), GeologistKitItem(GeologistKitLevel.LOW, FabricItemSettings().maxCount(4).maxDamage(100)))
		Registry.register(Registry.ITEM, Identifier("geologist", "geologist_kit2"), GeologistKitItem(GeologistKitLevel.MEDIUM, FabricItemSettings().maxCount(4).maxDamage(150)))
		Registry.register(Registry.ITEM, Identifier("geologist", "geologist_kit3"), GeologistKitItem(GeologistKitLevel.HIGH, FabricItemSettings().maxCount(4).maxDamage(200)))
	}

	override fun onInitialize() {
		val resolver: MappingResolver = FabricLoader.getInstance().mappingResolver
	}
}

