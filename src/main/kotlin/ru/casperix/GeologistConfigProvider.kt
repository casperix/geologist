package ru.casperix

import io.github.xn32.json5k.Json5
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import ru.casperix.util.createDirectoryForFile
import java.nio.file.Paths


object GeologistConfigProvider {
	private val path = Paths.get("config/geologist.json5")
	private var config = GeologistConfig()
	private val JSON = Json5 {
		encodeDefaults = true
		prettyPrint = true
	}


	fun loadAndGet(): GeologistConfig {
		val next = load()
		if (next == null) {
			save(config)
		} else {
			config = next
		}
		return get()
	}

	private fun load(): GeologistConfig? {
		val file = path.toFile()
		if (!file.exists()) return null

		return try {
			JSON.decodeFromString(file.readText())
		} catch (_: Throwable) {
			null
		}
	}

	private fun save(config: GeologistConfig) {
		val file = path.toFile()
		createDirectoryForFile(file)
		file.writeText(JSON.encodeToString(config))
	}

	fun get(): GeologistConfig {
		return config
	}

}