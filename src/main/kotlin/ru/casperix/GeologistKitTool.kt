package ru.casperix

import net.minecraft.item.Items
import net.minecraft.item.ToolMaterial
import net.minecraft.recipe.Ingredient

object GeologistKitTool : ToolMaterial {
	override fun getDurability(): Int {
		return 100
	}

	override fun getMiningSpeedMultiplier(): Float {
		return 1f
	}

	override fun getAttackDamage(): Float {
		return 0f
	}

	override fun getMiningLevel(): Int {
		return 0
	}

	override fun getEnchantability(): Int {
		return 0
	}

	override fun getRepairIngredient(): Ingredient {
		return Ingredient.ofItems(Items.WATER_BUCKET)
	}
}