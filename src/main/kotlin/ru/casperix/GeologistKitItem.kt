package ru.casperix

import net.minecraft.block.Block
import net.minecraft.block.Blocks
import net.minecraft.client.item.TooltipContext
import net.minecraft.entity.EquipmentSlot
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.*
import net.minecraft.sound.SoundEvents
import net.minecraft.text.MutableText
import net.minecraft.text.Text
import net.minecraft.util.*
import net.minecraft.util.registry.Registry
import net.minecraft.world.World
import ru.casperix.GeologistAnalyzer.analyze
import kotlin.math.max
import kotlin.math.min
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue


class GeologistKitItem(val level: GeologistKitLevel, settings: Settings?) : ToolItem(GeologistKitTool, settings) {
	fun changeOre(playerEntity: PlayerEntity, delta: Int) {
		val itemsAmount = getSupportedItems().size
		itemIndex = max(0, min(itemsAmount - 1, itemIndex + delta))

		getStateMessage()?.let {
			playerEntity.sendMessage(it, true)
		}
	}

	private fun getStateMessage(): MutableText? {
		val lastItem = getItem(itemIndex - 1)
		val currentItem = getItem(itemIndex) ?: return null
		val nextItem = getItem(itemIndex + 1)

		val lastValue = lastItem?.name?.formatted(Formatting.DARK_GRAY) ?: Text.translatable("key.geologist.nothing").formatted(Formatting.DARK_GRAY)
		val currentValue = currentItem.name.formatted(Formatting.BOLD).formatted(Formatting.GREEN)
		val nextValue = nextItem?.name?.formatted(Formatting.DARK_GRAY) ?: Text.translatable("key.geologist.nothing").formatted(Formatting.DARK_GRAY)
		return Text.translatable("item.geologist.geologist_kit.analysed.ready", lastValue, currentValue, nextValue)
	}

	private var itemIndex = 0

	private fun getBlockByIdentifier(name: String): Block? {
		val block = Registry.BLOCK.get(Identifier(name))
		if (block == Blocks.AIR) return null
		return block
	}


	private fun getSupportedItems(): List<Block> {
		val config = GeologistMod.config
		val kit = when (level) {
			GeologistKitLevel.LOW -> config.lowKit
			GeologistKitLevel.MEDIUM -> config.mediumKit
			GeologistKitLevel.HIGH -> config.highKit
		}

		return kit.allowedBlockNames.mapNotNull {
			getBlockByIdentifier(it)
		}
	}

	private fun getItem(index: Int): Block? {
		return getSupportedItems().getOrNull(index)
	}

	/**
	 * 	For debug only
	 */
//	override fun use(world: World, player: PlayerEntity, hand: Hand?): TypedActionResult<ItemStack> {
//		if (world.isClient) {
//			runOreAnalyze(world, player)
//		}
//
//		return super.use(world, player, hand)
//	}

	override fun useOnBlock(context: ItemUsageContext): ActionResult {
		val world = context.world
		val player = context.player


		if (player != null) {
			if (world.isClient) {
				runOreAnalyze(world, player)
			} else {
				val stack = context.stack
				stack.damage(1, player) {
					it.sendEquipmentBreakStatus(EquipmentSlot.MAINHAND)
				}
			}
		}

		return super.useOnBlock(context)
	}

	@OptIn(ExperimentalTime::class)
	fun runOreAnalyze(world: World, playerEntity: PlayerEntity) {
		val item = getItem(itemIndex) ?: return

		playerEntity.playSound(SoundEvents.BLOCK_BREWING_STAND_BREW, 1.0f, 1.0f)

		val (percent, time) = measureTimedValue {
			analyze(world, playerEntity.blockPos, item)
		}
		GeologistMod.logger.info("Measured for ${time.toLong(DurationUnit.MILLISECONDS)}ms")


		val valueText = Text.literal(String.format("%.1f%%", percent)).formatted(Formatting.WHITE)
		val output = Text.translatable("item.geologist.geologist_kit.analysed.item", valueText, item.name).formatted(Formatting.GRAY)
		playerEntity.sendMessage(output, true)
	}

	override fun appendTooltip(itemStack: ItemStack?, world: World?, tooltip: MutableList<Text?>, tooltipContext: TooltipContext?) {
//		tooltip.add(Text.translatable("item.geologist.geologist_kit.tooltip.title.${level.index}").formatted(Formatting.WHITE).formatted(Formatting.BOLD))
		tooltip.add(Text.translatable("item.geologist.geologist_kit.tooltip.main").formatted(Formatting.GRAY))
	}

}

