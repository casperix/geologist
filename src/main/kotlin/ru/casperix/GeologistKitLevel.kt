package ru.casperix

enum class GeologistKitLevel(val index:Int) {
	LOW(1),
	MEDIUM(2),
	HIGH(3)
}