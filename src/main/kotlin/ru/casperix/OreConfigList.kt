package ru.casperix

object OreConfigList {
	val LOW = listOf(
		"minecraft:copper_ore",
		"minecraft:iron_ore",
		"minecraft:gold_ore",
		"minecraft:nether_gold_ore",

		"create:zinc_ore",

		"techreborn:bauxite_ore",
		"techreborn:tin_ore",
		"techreborn:lead_ore",
		"techreborn:galena_ore",
		"techreborn:silver_ore",

		"indrev:tin_ore",
		"indrev:nikolite_ore",
		"indrev:silver_ore",
		"indrev:lead_ore",

		"modern_industrialization:lignite_coal_ore",
		"modern_industrialization:bauxite_ore",
		"modern_industrialization:tin_ore",
		"modern_industrialization:antimony_ore",
		"modern_industrialization:nickel_ore",
		"modern_industrialization:lead_ore",
	)

	val MEDIUM = listOf(
		"minecraft:coal_ore",
		"minecraft:redstone_ore",
		"minecraft:lapis_ore",
		"minecraft:diamond_ore",
		"minecraft:nether_quartz_ore",

		"techreborn:ruby_ore",
		"techreborn:sapphire_ore",
		"techreborn:iridium_ore",

		"modern_industrialization:salt_ore",
		"modern_industrialization:uranium_ore",
		"modern_industrialization:tungsten_ore",
		"modern_industrialization:mozanite_ore",
		"modern_industrialization:iridium_ore",
		)

	val HIGH = listOf(
		"minecraft:andesite",
		"minecraft:diorite",
		"minecraft:granite",
		"minecraft:calcite",

		"blockus:bluestone",
		"blockus:limestone",
		"blockus:marble",
		"blockus:viridite",

		"create:crimsite",
		"create:asurine",
		"create:veridium",
		"create:scoria",
		"create:limestone",
	)
}