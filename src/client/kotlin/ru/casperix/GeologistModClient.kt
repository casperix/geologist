package ru.casperix

import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper
import net.minecraft.client.option.KeyBinding
import net.minecraft.client.util.InputUtil
import org.lwjgl.glfw.GLFW

object GeologistModClient : ClientModInitializer {
	override fun onInitializeClient() {
		GeologistMod.config = GeologistConfigProvider.loadAndGet()

		val lastItem = KeyBindingHelper.registerKeyBinding(
			KeyBinding(
				"key.geologist.last_item",  // The translation key of the keybinding's name
				InputUtil.Type.KEYSYM,  // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
				GLFW.GLFW_KEY_LEFT,  // The keycode of the key
				"category.geologist.mod" // The translation key of the keybinding's category.
			)
		)

		val nextItem = KeyBindingHelper.registerKeyBinding(
			KeyBinding(
				"key.geologist.next_item",  // The translation key of the keybinding's name
				InputUtil.Type.KEYSYM,  // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
				GLFW.GLFW_KEY_RIGHT,  // The keycode of the key
				"category.geologist.mod" // The translation key of the keybinding's category.
			)
		)

		ClientTickEvents.END_CLIENT_TICK.register(ClientTickEvents.EndTick { client ->
			val playerEntity = client.player
			playerEntity?.handItems?.forEach { stack ->
				val item = stack.item
				if (item is GeologistKitItem) {
					if (lastItem.wasPressed()) {
						item.changeOre(playerEntity, -1)
					}
					if (nextItem.wasPressed()) {
						item.changeOre(playerEntity, 1)
					}
				}
			}
		})
	}
}